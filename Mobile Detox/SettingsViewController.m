//
//  SettingsViewController.m
//  Mobile Detox
//
//  Created by Glenn Thrope on 5/25/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import "UserStore.h"
#import "AppDelegate.h"
#import "SettingsViewController.h"

@implementation SettingsViewController

/*
- (void)viewDidLoad {
    [super viewDidLoad];

    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
    for(NSHTTPCookie *cookie in cookies) {

        NSLog(@"%@ %@ %@ %@ %@ %@", cookie.name, cookie.value, cookie.domain, cookie.path, cookie.sessionOnly ? @"true" : @"false", cookie.HTTPOnly ? @"true" : @"false");

    }
}*/

- (IBAction)logOut {
    
    NSURL *url = [NSURL URLWithString:@"http://localhost:3000/sessions"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"DELETE";
    
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        [self logOutRequestCompleteWithData:data response:response error:error];
    }];
    [task resume];
}

-(void)logOutRequestCompleteWithData:(NSData *)data
                            response:(NSURLResponse *)response
                               error:(NSError *)error {
 
    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
    NSLog(@"Status code: %ld", (long)urlResponse.statusCode);

    [UserStore sharedStore].name = nil;
    [UserStore sharedStore].email = nil;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        [appDelegate setRootViewController:NO];
    });
}



@end
