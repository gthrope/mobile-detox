//
//  LoginViewController.m
//  Mobile Detox
//
//  Created by Glenn Thrope on 3/30/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import "UserStore.h"
#import "AppDelegate.h"
#import "LoginViewController.h"

@interface LoginViewController ()

@property (nonatomic, weak) IBOutlet UITextField *email;
@property (nonatomic, weak) IBOutlet UITextField *password;

@end

@implementation LoginViewController

// consider pulling out common functionality with SignUpViewController.signup
-(IBAction)login:(id)sender {
    
    /* create json body to post to server */
    
    NSString *email = self.email.text;
    NSString *password = self.password.text;
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                email, @"email",
                                password, @"password", nil];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:nil];
    
    /* post data to server */
    
    NSURL *url = [NSURL URLWithString:@"http://localhost:3000/sessions"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    request.HTTPBody = jsonData;
    
    NSURLSessionDataTask *task = [[NSURLSession sharedSession]
                                  dataTaskWithRequest:request
                                  completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                      [self loginRequestCompleteWithData:data response:response error:error];
                                  }];
    [task resume];    
}

-(void)loginRequestCompleteWithData:(NSData *)data
                           response:(NSURLResponse *)response
                              error:(NSError *)error {
    
    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
    NSLog(@"Status code: %ld", (long)urlResponse.statusCode);
    
    if(urlResponse.statusCode == 200) {
    
        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        [UserStore sharedStore].name = jsonData[@"name"];
        [UserStore sharedStore].email = jsonData[@"email"];
        
        // login successful
        // have to do this on main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
            [appDelegate setRootViewController:YES];
        });
        
    }
    else {
        NSString *error = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"Error: %@", error);

        // have to do this on main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Whoops..." message:error delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });

    }
}

@end
