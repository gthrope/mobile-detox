//
//  AppDelegate.h
//  Mobile Detox
//
//  Created by Glenn Thrope on 3/28/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

-(void)setRootViewController:(BOOL)isLoggedIn;

@end

