//
//  UserStore.m
//  Mobile Detox
//
//  Created by Glenn Thrope on 5/27/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import "UserStore.h"

@implementation UserStore

+ (instancetype)sharedStore {
    
    static UserStore *sharedStore;
    
    if(!sharedStore) {

        NSString *path = [UserStore itemArchivePath];
        sharedStore = [NSKeyedUnarchiver unarchiveObjectWithFile:path];

        if(!sharedStore) {
            
            sharedStore = [[UserStore alloc] initPrivate];
        }
    }
    
    return sharedStore;
}

- (instancetype)initPrivate {

    return [super init];
}

- (instancetype)init {
    @throw [NSException exceptionWithName:@"Singleton"
                                   reason:@"Use +[UserStore sharedStore]"
                                 userInfo:nil];
    return nil;
}

#pragma mark - Archiving methods

+(NSString *)itemArchivePath {
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                       NSUserDomainMask, YES);
    
    NSString *documentDirectory = [documentDirectories firstObject];
    
    return [documentDirectory stringByAppendingPathComponent:@"items.archive"];
}

-(BOOL)saveChanges {
    
    NSString *path = [UserStore itemArchivePath];
    return [NSKeyedArchiver archiveRootObject:self toFile:path];
}

#pragma mark - NSCoding methods

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super init];
    if(self) {
        
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.email = [aDecoder decodeObjectForKey:@"email"];
    }
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.email forKey:@"email"];
}


@end
