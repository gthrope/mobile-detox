//
//  UserStore.h
//  Mobile Detox
//
//  Created by Glenn Thrope on 5/27/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserStore : NSObject <NSCoding>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *email;

+ (instancetype)sharedStore;
- (BOOL)saveChanges;

@end
