//
//  SignUpViewController.m
//  Mobile Detox
//
//  Created by Glenn Thrope on 3/30/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import "UserStore.h"
#import "AppDelegate.h"
#import "SignUpViewController.h"

@interface SignUpViewController ()

@property (nonatomic, weak) IBOutlet UITextField *name;
@property (nonatomic, weak) IBOutlet UITextField *email;
@property (nonatomic, weak) IBOutlet UITextField *password;
@property (nonatomic, weak) IBOutlet UITextField *confirmation;

@end

@implementation SignUpViewController

// consider pulling out common functionality with LoginViewController.login
-(IBAction)signup:(id)sender {

    /* create json body to post to server */
    
    NSString *name = self.name.text;
    NSString *email = self.email.text;
    NSString *password = self.password.text;
    NSString *confirmation = self.confirmation.text;
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                name, @"name",
                                email, @"email",
                                password, @"password",
                                confirmation, @"password_confirmation", nil];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:nil];
    
    /* post data to server */
    
    NSURL *url = [NSURL URLWithString:@"http://localhost:3000/users"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    request.HTTPBody = jsonData;

    NSURLSessionDataTask *task = [[NSURLSession sharedSession]
                                  dataTaskWithRequest:request
                                  completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                      [self signUpRequestCompleteWithData:data response:response error:error];
                                  }];
    [task resume];
}
                                  
                                  
-(void)signUpRequestCompleteWithData:(NSData *)data
                            response:(NSURLResponse *)response
                               error:(NSError *)error {

    NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
    NSLog(@"Status code: %ld", (long)urlResponse.statusCode);
    
    if(urlResponse.statusCode == 200) {

        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];        
        [UserStore sharedStore].name = jsonData[@"name"];
        [UserStore sharedStore].email = jsonData[@"email"];
        
        // have to do this on main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
            [appDelegate setRootViewController:YES];
        });
        
    }
    else {
        NSString *error = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"Error: %@", error);
        
        // have to do this on main thread
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Whoops..." message:error delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        });
        
    }
};


@end
