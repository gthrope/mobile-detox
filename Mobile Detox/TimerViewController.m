//
//  ViewController.m
//  Mobile Detox
//
//  Created by Glenn Thrope on 3/28/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import "AppDelegate.h"
#import "TimerViewController.h"

@interface TimerViewController () {
    int seconds;
}

@property (nonatomic, weak) IBOutlet UILabel *timerLabel;

@end

@implementation TimerViewController

- (IBAction)startTimer:(id)sender {

    seconds = 60 * 5;
    NSTimer *timer = [NSTimer timerWithTimeInterval:1.0
                                    target:self
                                  selector:@selector(timerDidTick:)
                                  userInfo:nil
                                   repeats:YES];
    
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appDidEnterBackground)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
}

- (IBAction)logout:(id)sender {
    
    // logout - FIX
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:@"isLoggedIn"];
    [defaults synchronize];
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate setRootViewController:NO];
}

- (void)timerDidTick:(NSTimer *)timer {

    seconds--;
    
    NSString *newTime = [NSString stringWithFormat:@"%01d:%02d", seconds / 60, seconds % 60];
    self.timerLabel.text = newTime;
    
    if(seconds == 0) {
        [timer invalidate];
    }
}

-(void)appDidEnterBackground {

    UILocalNotification *warning = [[UILocalNotification alloc] init];
    warning.alertBody = @"You didn't make it through your detox.";
    [[UIApplication sharedApplication] presentLocalNotificationNow:warning];
}

@end
