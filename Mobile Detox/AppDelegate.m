//
//  AppDelegate.m
//  Mobile Detox
//
//  Created by Glenn Thrope on 3/28/15.
//  Copyright (c) 2015 Thrope Inc. All rights reserved.
//

#import "UserStore.h"
#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    // request notification permissions
    if([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]) {
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert categories:nil]];
    }

    BOOL isLoggedIn = NO;
    
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
    for(NSHTTPCookie *cookie in cookies) {
        if([cookie.name isEqualToString:@"remember_token"]) {
            isLoggedIn = YES;
            break;
        }
    }
    
    [self setRootViewController:isLoggedIn];
    
    return YES;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    BOOL success = [[UserStore sharedStore] saveChanges];
    if (success) {
        NSLog(@"Saved all of the UserStore");
    } else {
        NSLog(@"Could not save any of the UserStore");
    }
}

-(void)setRootViewController:(BOOL)isLoggedIn {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *rootViewController;
    
    if(isLoggedIn) {
        rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"loggedInViewController"];
    }
    else {
        rootViewController = [storyboard instantiateInitialViewController];
    }
    
    self.window.rootViewController = rootViewController;
    
}

@end
